
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.awt.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class linear_reg {

public static void main(String[] args) {
// Call method which helps to generate the table
generateJTable();
}

/**
* This method helps to generate Table with buttons like Add, Update, Delete
*/
public static void generateJTable() {

	
	String dates[] = new String[100];
	/////////////// load datada
	String fileName = "/home/jimc74/Documents/scores.txt";

	  long ndays[] = new long[100];
	double data[][] = new double[100][100];
	  Date fdate = new GregorianCalendar(2000, 11, 31, 23, 59).getTime();

	  Date sdate = new GregorianCalendar(2000, 11, 31, 23, 59).getTime();
	  
	
	
	String fileName2 = "/home/jimc74/Documents/scores.txt";
    
    int linecount = 0;
    
     try {
           // FileReader reads text files in the default encoding.
           FileReader fileReader = 
               new FileReader(fileName2);

           // Always wrap FileReader in BufferedReader.
           BufferedReader bufferedReader = 
               new BufferedReader(fileReader);

           
           int countx1 = 0;
           int countx2 = 0;
           int firstdate = 0;
           String sfirstdate = "";
           
           
           String line = "";
           int count = 0;
           int count2 = 0;
        //   String line;
        
         int xz = -1;
		while((line = bufferedReader.readLine()) != null) {
      //for ( int xz = 0 ; xz <= 12; xz++)   { 
    	 // line = bufferedReader.readLine();
          //`[countx1][countx2] = line;
    	  
    	  System.out.println("read-->" + line);
		//	dtm.setValueAt(line, countx1, countx2);
          xz += 1;
		
			if ( xz % 3 == 2 )
			{
				System.out.println("[][][][]][");
				System.out.println(line);
				
				data[linecount][1] = Integer.parseInt(line);
			    linecount += 1;
			   
			}
			   
			
           countx2++;
           
           if (firstdate == 1)
           {
        	   sfirstdate = line;
        	   fdate.setMonth(1);
        	   fdate.setDate(1);
        	   fdate.setYear(2017);
        	   
        //	   sdate.setMonth(2);
        	//   sdate.setDate(3);
        	//   sdate.setYear(2017);
        	//   long diff = sdate.getTime() - fdate.getTime();
        	   
   		//	int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
   	//		System.out.println("difference between days: " + diffDays); 

      	     
      	     // Get msec from each, and subtract.
      	//     long diff = today.getTime() - d1.getTime();
           }
           firstdate++;
           System.out.println("firstdate:" + sfirstdate);
           
           
           
          if ( countx2 == 2)
          {
        	System.out.println("*************----------");
        	  sdate.setYear(2017);
        	  
        	  System.out.println("line -->" + line);
        	  String[] output = line.split("/");
        		
        	  
        	  
        		sdate.setMonth(Integer.parseInt(output[0]));
            	  sdate.setDate(Integer.parseInt(output[1]));
            	  sdate.setYear(Integer.parseInt(output[2]));
        		
            	  long diff = sdate.getDate() - fdate.getDate();
            	   
  	       			//int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
  	       			System.out.println("difference between days: " + diff); 
                System.out.println(diff);
        		ndays[countx1] = diff;
        		//data[countx1][0] = diff;
        	    dates[countx1] = line;
          }
        	
          
           
           if (countx2 > 2)
           {
        	   countx2 = 0;
        	   countx1 += 1;
           }
           
           
        	
        	   
           System.out.println(line);
           
           
           }
           // Always close files.
           bufferedReader.close();         
       }
       catch(FileNotFoundException ex) {
           System.out.println(
               "Unable to open file '" + 
               fileName + "'");                
       }
       catch(IOException ex) {
           System.out.println(
               "Error reading file '" 
               + fileName + "'");                  
           // Or we could just do this: 

           // ex.printStackTrace();
       


 	
       }
 //   bw.close();
//      fw.close();

	
	///////////////
	
	
// create JFrame and JTable
JFrame frame = new JFrame();
final JTable table = new JTable();

// create a table model and set a Column Identifiers to this model
Object[] columns = { "Record #", "Date", "Score" };
final DefaultTableModel model = new DefaultTableModel();
model.setColumnIdentifiers(columns);

// set the model to the table
table.setModel(model);

// Change A JTable Background Color, Font Size, Font Color, Row Height
table.setBackground(Color.CYAN.brighter());
table.setForeground(Color.black);
Font font = new Font("", 1, 18);
table.setFont(font);
table.setRowHeight(30);

// create JTextFields to hold the value
final JTextField textId = new JTextField();
final JTextField textFname = new JTextField();
final JTextField textLname = new JTextField();
final JTextField textAge = new JTextField();

// create JButtons to add the action
JButton btnAdd = new JButton("Add");
JButton btnDelete = new JButton("Delete");
JButton btnUpdate = new JButton("Update");

JButton btnDraw = new JButton("Draw");

textId.setBounds(20, 220, 100, 25);
textFname.setBounds(20, 250, 100, 25);
textLname.setBounds(20, 280, 100, 25);
textAge.setBounds(20, 310, 100, 25);

btnAdd.setBounds(150, 220, 100, 25);
btnUpdate.setBounds(150, 265, 100, 25);
btnDelete.setBounds(150, 310, 100, 25);

btnDraw.setBounds(10,230, 75, 75);

// create JScrollPane
JScrollPane pane = new JScrollPane(table);
pane.setBounds(10, 10, 600, 200);

frame.setLayout(null);

frame.add(pane);

// add JTextFields to the jframe
frame.add(textId);
textId.setLocation(210, 220);
frame.add(textFname);
textFname.setLocation(210, 250);
frame.add(textLname);
textLname.setLocation( 210, 280);
frame.add(textAge);
textAge.setLocation(210, 310);


// add JButtons to the jframe
frame.add(btnAdd);
btnAdd.setLocation(350,220);
frame.add(btnDelete);
btnDelete.setLocation(350, 250);
frame.add(btnUpdate);
btnUpdate.setLocation(350, 280);

frame.add(btnDraw);

// create an array of objects to set the row data
final Object[] row = new Object[4];


for ( int xx = 0; xx < 4; xx++)
{
	row[0] = xx;
	row[1] = dates[xx];
	row[2] = data[xx][1];
	
	
	System.out.println("------------------------------------>" + row[1]);
	model.addRow(row);
}





// button add row - Clicked on Add Button
btnAdd.addActionListener(new ActionListener() {

@Override
public void actionPerformed(ActionEvent e) {

row[0] = textId.getText();
row[1] = textFname.getText();
row[2] = textLname.getText();
row[3] = textAge.getText();

// add row to the model
model.addRow(row);
}
});

// button remove row - Clicked on Delete Button
btnDelete.addActionListener(new ActionListener() {

@Override
public void actionPerformed(ActionEvent e) {

// i = the index of the selected row
int i = table.getSelectedRow();
if (i >= 0) {
// remove a row from jtable
model.removeRow(i);
} else {
System.out
.println("There were issue while Deleting the Row(s).");
}
}
});

// get selected row data From table to textfields
table.addMouseListener(new MouseAdapter() {

@Override
public void mouseClicked(MouseEvent e) {

// i = the index of the selected row
int i = table.getSelectedRow();

textId.setText(model.getValueAt(i, 0).toString());
textFname.setText(model.getValueAt(i, 1).toString());
textLname.setText(model.getValueAt(i, 2).toString());
textAge.setText(model.getValueAt(i, 3).toString());
}
});

// button update row - Clicked on Update Button
btnUpdate.addActionListener(new ActionListener() {

@Override
public void actionPerformed(ActionEvent e) {

// i = the index of the selected row
int i = table.getSelectedRow();

if (i >= 0) {
model.setValueAt(textId.getText(), i, 0);
model.setValueAt(textFname.getText(), i, 1);
model.setValueAt(textLname.getText(), i, 2);
model.setValueAt(textAge.getText(), i, 3);
} else {
System.out.println("Update Error");
}
}
});

frame.setSize(900, 400);
frame.setLocationRelativeTo(null);
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setVisible(true);

}
